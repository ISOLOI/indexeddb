# README #

Customer Manager Database utilizing the IndexedDB API.

IndexedDB is a low-level API for client-side storage of significant amounts of structured data, including files/blobs. This API uses indexes to enable high performance searches of this data

This is a javascript project implementing jQuery.



### Usage ###

* Customer Manager DB allows user to insert a customer name and email onto the dashboard interface. The user may also delete a name, edit a name, or clear the DB without refreshing the page.
* v0.01


### How do I get set up? ###

* Clone Repository to desktop, launch in LAMP or XAMPP 'htdocs' folder.
- Use Side Menu Nave to Add Customer's, and view list of DB.



### Coded By ###

* Nick Kaufmann **coding-saints.com**